<?php get_header(); ?>

<section id="content">
	<div class="container">
		<div class="row content-wrapper">

			<?php get_sidebar(); ?>

			<div class="<?php echo is_active_sidebar('sidebar-1') == true ? 'col-lg-8 col-md-8 col-sm-6 col-xs-12' : 'col-lg-12 col-md-12 col-sm-12 col-xs-12'; ?> content">
				
				<img src="<?php echo get_template_directory_uri(); ?>/img/404.png" class="img-responsive center-block" alt="Not found image" width="" height="" />
				<?php get_search_form(); ?>

			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>