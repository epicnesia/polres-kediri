<?php get_header(); ?>

<section id="content">
	<div class="container">
		<div class="row content-wrapper">

			<?php get_sidebar(); ?>

			<div class="<?php echo is_active_sidebar('sidebar-1') == true ? 'col-lg-8 col-md-8 col-sm-6 col-xs-12' : 'col-lg-12 col-md-12 col-sm-12 col-xs-12'; ?> content">

				<?php if (have_posts()) : while (have_posts()) : the_post(); 
				
					$the_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'thumbnail');
				
				?>
				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 list-archive">
					
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<?php if(isset($the_url[0])) : ?>
						
							<img class="img-responsive" src="<?php echo $the_url[0]; ?>" width="250" height="150" alt="Featured image of <?php the_title(); ?>" />
							
						<?php else: ?>
								
							<img class="img-responsive" src="<?php echo get_template_directory_uri() . '/img/placeholder.jpg'; ?>" width="250" height="150" alt="Featured image of <?php the_title(); ?>" />
								
						<?php endif; ?>
						</div>
						<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
							<div class="category-list pull-left">
							<?php
								global $post;
								$categories = get_the_category($post->ID);
								
								if ($categories) {
								
									foreach ($categories as $category) {
										?>
										
										<span><?php echo $category->name; ?></span>
										
										<?php
									}
								}
							?>
							</div>
							<a href="<?php echo get_permalink(); ?>" class="post-title"><h4><?php the_title(); ?></h4></a>
							By <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author_meta('display_name'); ?></a> /
							<i class="post-time"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> ago</i>
						</div>
						
					</div>
					
				</div>
				
				<?php endwhile; ?>
				<?php endif; ?>

			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>