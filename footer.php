	<div class="container">
		<!-- Start Footer -->
		<footer class="row">
			<div id="footer" class="col-lg-12">
				<p class="text-center">
					Copyright © <?php the_time('Y'); ?>
					<a href="#"><?php bloginfo('name'); ?></a>.
					All Rights Reserved.
				</p>
			</div>
		</footer>
		<!-- End Footer -->
	</div>
	
	</body>
</html>
<?php wp_footer(); ?>