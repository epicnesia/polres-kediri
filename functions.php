<?php

/**
 * =========================================================================================
 * Polres Kediri template functions and definitions
 * =========================================================================================
 */
 
 /* ------------------------------------------------------------------------- *
 *  OptionTree framework integration: Use in theme mode
/* ------------------------------------------------------------------------- */
 
	add_filter( 'ot_show_pages', '__return_false' );
	add_filter( 'ot_show_new_layout', '__return_false' );
	add_filter( 'ot_theme_mode', '__return_true' );
 
/**
 * Required: include OptionTree.
 */
include_once( 'option-tree/ot-loader.php' );
/**
 * Theme Options
 */
include_once( 'inc/theme-options/theme-options.php' );

require_once ('inc/wp_bootstrap_navwalker/wp_bootstrap_navwalker.php');
// Bootstrap Nav Walker

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since polreskediri 1.0
 */
if (!function_exists('polreskediri_setup')) :
	function polreskediri_setup() {

		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support('title-tag');

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		add_theme_support('post-thumbnails');

		// Register navigation menu
		register_nav_menu('primary', __('Primary navigation', 'polreskediri'));

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support('html5', array('search-form', 'comment-form', 'comment-list', 'gallery', 'caption', ));
	}

endif; // polreskediri_setup
add_action('after_setup_theme', 'polreskediri_setup');

if (!function_exists('polreskediri_fonts_url')) :
	/**
	 * Register Google fonts for Polres Kediri Theme.
	 *
	 * Create your own polreskediri_fonts_url() function to override in a child theme.
	 *
	 * @since polreskediri 1.0
	 *
	 * @return string Google fonts URL for the theme.
	 */
	function polreskediri_fonts_url() {
		$fonts_url = '';
		$fonts = array();
		$subsets = 'latin,latin-ext';

		/* translators: If there are characters in your language that are not supported by Nunito, translate this to 'off'. Do not translate into your own language. */
		if ('off' !== _x('on', 'Nunito font: on or off', 'polreskediri')) {
			$fonts[] = 'Nunito';
		}

		/* translators: If there are characters in your language that are not supported by Roboto, translate this to 'off'. Do not translate into your own language. */
		if ('off' !== _x('on', 'Roboto font: on or off', 'polreskediri')) {
			$fonts[] = 'Roboto';
		}

		if ($fonts) {
			$fonts_url = add_query_arg(array('family' => urlencode(implode('|', $fonts)), 'subset' => urlencode($subsets), ), 'https://fonts.googleapis.com/css');
		}

		return $fonts_url;
	}

endif;

/**
 * Enqueues scripts and styles.
 *
 * @since polreskediri 1.0
 */
function polreskediri_scripts() {
	// Bootstrap framework
	wp_enqueue_style('bootstrap-main', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.6');

	// Normalize framework
	wp_enqueue_style('normalize-css', get_template_directory_uri() . '/css/normalize.css', array(), '4.1.1');

	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style('polreskediri-fonts', polreskediri_fonts_url(), array(), null);

	// Add Fontawesome.
	wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.4.0');

	// Add Hover CSS, used in the main stylesheet.
	wp_enqueue_style('hover-css', get_template_directory_uri() . '/css/hover.min.css', array(), '2.0.1');

	// Add Nivo Slider CSS.
	wp_enqueue_style('nivo-slider-css', get_template_directory_uri() . '/inc/nivo_slider/nivo-slider.css', array(), '3.2');
	wp_enqueue_style('nivo-slider-theme-css', get_template_directory_uri() . '/inc/nivo_slider/themes/default/default.css', array(), '1.3');
	wp_enqueue_style('nivo-slider-custom-css', get_template_directory_uri() . '/inc/nivo_slider/nivo-custom.css', array(), '3.2');

	// Add jQuery Slick Carousel CSS.
	wp_enqueue_style('slick-carousel-css', get_template_directory_uri() . '/inc/slick/slick.css', array(), '1.6.0');
	wp_enqueue_style('slick-carousel-theme-css', get_template_directory_uri() . '/inc/slick/slick-theme.css', array(), '1.6.0');

	// Theme stylesheet.
	wp_enqueue_style('polreskediri-style', get_stylesheet_uri());

	// Load jQuery library
	wp_enqueue_script('polreskediri-jquery-main', get_template_directory_uri() . '/js/jquery-2.2.4.min.js', array(), '2.2.4');

	// Load bootstrap js
	wp_enqueue_script('polreskediri-bootstrap-script', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '3.3.6');

	// Load jQuery easing
	wp_enqueue_script('polreskediri-jquery-easing', get_template_directory_uri() . '/js/jquery.easing.1.3.js', array(), '1.3');

	// Load jQuery Nivo Slider Scripts
	wp_enqueue_script('nivo-slider-js', get_template_directory_uri() . '/inc/nivo_slider/jquery.nivo.slider.js', array(), '3.2');
	wp_enqueue_script('nivo-slider-setting-js', get_template_directory_uri() . '/inc/nivo_slider/setting.js', array(), '3.2');

	// Load jQuery Slick Carousel Scripts
	wp_enqueue_script('slick-carousel-js', get_template_directory_uri() . '/inc/slick/slick.min.js', array(), '3.2');
	wp_enqueue_script('slick-carousel-setting-js', get_template_directory_uri() . '/inc/slick/setting.js', array(), '3.2');
	
	// Load the html5 shiv.
	wp_enqueue_script('polreskediri-html5', get_template_directory_uri() . '/js/html5shiv.min.js', array(), '3.7.2');
	wp_script_add_data('polreskediri-html5', 'conditional', 'lt IE 9');

	// Load the respond.js.
	wp_enqueue_script('polreskediri-respond', get_template_directory_uri() . '/js/respond.min.js', array(), '1.4.2');
	wp_script_add_data('polreskediri-respond', 'conditional', 'lt IE 9');

	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}

	// Load jQuery customs script
	wp_enqueue_script('polreskediri-jquery-custom', get_template_directory_uri() . '/js/custom.js', array(), '1.0');
}

add_action('wp_enqueue_scripts', 'polreskediri_scripts');

// Nivo Slider Function
function nivo_slider($type = 'nivo_standart') {
	
	$cat_name = $type == 'nivo_widget' ? 'headlines-widget' : 'headlines';
	$row_type = $type == 'nivo_widget' ? '' : 'row';
	$wrapper_id = $type == 'nivo_widget' ? 'widget-slider' : 'slider';
	
	if($type == 'nivo_widget'){
		$args = function_exists( 'ot_get_option' ) ? array('cat' => ot_get_option('cat_headlines_widget')) : array('category_name' => $cat_name);
	} else {
		$args = function_exists( 'ot_get_option' ) ? array('cat' => ot_get_option('cat_headlines')) : array('category_name' => $cat_name);
	}

	$result = '<div class="'.$row_type.' theme-default">';
	$result .= '<div id="' . $wrapper_id . '" class="nivoSlider">';

	//the loop
	$loop = new WP_Query($args);
	while ($loop -> have_posts()) {
		global $post;
		$loop -> the_post();

		$the_url = wp_get_attachment_image_src(get_post_thumbnail_id($post -> ID), $type);
		$img_src = isset($the_url[0]) ? $the_url[0] : get_template_directory_uri() . '/img/placeholder.jpg';
		if($type == 'nivo_widget'){
			$result .= '<a href="' . get_the_permalink() . '"><img title="#caption' . get_the_ID() . '" src="' . $img_src . '" data-thumb="' . $img_src . '" alt="' . get_the_title() . '"/></a>';
		}else{
			$result .= '<img title="#caption' . get_the_ID() . '" src="' . $img_src . '" data-thumb="' . $img_src . '" alt="' . get_the_title() . '"/>';
		}
		
	}
	$result .= '</div>';
	//the caption loop
	$loop = new WP_Query($args);
	while ($loop -> have_posts()) {
		$loop -> the_post();

		$result .= '<div id="caption' . get_the_ID() . '" class="nivo-html-caption">';
		$result .= '<h3>' . get_the_title() . '</h3><p>' . my_excerpt(get_the_content(), 35) . '</p>';
		$result .= '<a class="slider-link" href="' . get_the_permalink() . '">Read More</a>';
		$result .= '</div>';
	}
	$result .= '</div>';
	return $result;
}

// My Excerpt Function
function my_excerpt($str, $count=100, $type='word') {
	
	if($type == 'letter'){
		$excerpt = substr($str, 0, $count);
	} else {
		$excerpt_array = explode(" ", strip_tags($str));
		$excerpt = "";
		for ($s = 0; $s < $count; $s++) {
			$excerpt .= $excerpt_array[$s];
	
			if ($s < count($excerpt_array) || $s < $word_count) {
				$excerpt .= " ";
			}
		}
	}
	
	return $excerpt.'[...]';
}

// My Date Function
function my_date($date, $type='full') {
	
	if($type == 'day'){
		if(strlen($date) >= 10){
			return substr($date, 0, 2);
		}else{
			return '0'.substr($date, 0, 1);
		}
	}elseif($type == 'month'){
		if(strlen($date) >= 10){
			return substr($date, 3, 2);
		}else{
			return substr($date, 2, 2);
		}
	}elseif($type == 'year'){
		if(strlen($date) >= 10){
			return substr($date, 6);
		}else{
			return substr($date, 5);
		}
	}elseif($type == 'full'){
		if(strlen($date) >= 10){
			return $date;
		}else{
			return '0'.$date;
		}
	}
	
}

// Widgets Sidebar Init
function polreskediri_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar Widget Area', 'polreskediri' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Appears in the sidebar section of the site.', 'polreskediri' ),
		'before_widget' => '<div class="col-lg-12">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
}
add_action( 'widgets_init', 'polreskediri_widgets_init' );

// Comments Function
if ( ! function_exists( 'polreskediri_comment' ) ) :
	
function polreskediri_comment( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;
    switch ( $comment->comment_type ) :
        case 'pingback' :
        case 'trackback' :
    ?>
    <li class="post pingback">
        <p><?php _e( 'Pingback:', 'polreskediri' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( '(Edit)', 'polreskediri' ), ' ' ); ?></p>
    <?php
            break;
        default :
    ?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
        <article id="comment-<?php comment_ID(); ?>" class="comment">
            <footer>
                <div class="comment-author vcard">
                    <?php echo get_avatar( $comment, 40 ); ?>
                    <?php printf( __( '%s <span class="says">says:</span>', 'polreskediri' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
                </div><!-- .comment-author .vcard -->
                <?php if ( $comment->comment_approved == '0' ) : ?>
                    <em><?php _e( 'Your comment is awaiting moderation.', 'polreskediri' ); ?></em>
                    <br />
                <?php endif; ?>
 
                <div class="comment-meta commentmetadata">
                    <a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>"><time pubdate datetime="<?php comment_time( 'c' ); ?>">
                    <?php
                        /* translators: 1: date, 2: time */
                        printf( __( '%1$s at %2$s', 'polreskediri' ), get_comment_date(), get_comment_time() ); ?>
                    </time></a>
                    <?php edit_comment_link( __( '(Edit)', 'polreskediri' ), ' ' );
                    ?>
                </div><!-- .comment-meta .commentmetadata -->
            </footer>
 
            <div class="comment-content"><?php comment_text(); ?></div>
 
            <div class="reply">
                <?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
            </div><!-- .reply -->
        </article><!-- #comment-## -->
 
    <?php
            break;
    endswitch;
}
endif; // ends check for polreskediri_comment()

// Get Recent Post From Another DB
function get_recent_news($thumb_size = '150x150') {
	
	$mydb = new wpdb('root','','smada','localhost');
	$rows = $mydb->get_results("SELECT ID, post_name, post_title FROM wp_posts WHERE post_status='publish' ORDER BY post_date DESC LIMIT 6");
	
	// get home url
	$home_url = '';
	$home = $mydb->get_results("SELECT option_value FROM wp_options WHERE option_name='siteurl'");
	foreach($home as $home){ $home_url = $home->option_value; }
	
	$result = '';
	foreach ($rows as $obj) {
		
		$pm = $mydb->get_results("SELECT meta_value FROM wp_postmeta WHERE meta_key='_thumbnail_id' AND post_id='$obj->ID'");
		
		$img_url = get_template_directory_uri() . '/img/placeholder.jpg';
		
		foreach($pm as $pm_obj) {
			
			$t = $mydb->get_results("SELECT guid FROM wp_posts WHERE ID='$pm_obj->meta_value'");
			
			foreach ($t as $t_obj) {
				$ext = substr($t_obj->guid, -4);
				$thumb_url = str_replace($ext, '', $t_obj->guid);
				if(isset($t_obj->guid)) $img_url = $thumb_url.'-'.$thumb_size.$ext;
			}
			
		}
		
	   	$result .= '<div class="tribrata-item">
						<a href="'.esc_url($home_url).'/'.$obj->post_name.'">
							<img class="pull-left" src="'.$img_url.'" alt="Placeholder Image" width="" height="" />
							<span class="tribrata-item-title">'.$obj->post_title.'</span>
						</a>
					</div>';
	}
	
	return $result;
}

?>