<!DOCTYPE html>
<html <?php language_attributes() ?>>
	<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title><?php
	if (function_exists('ot_get_option')) { echo ot_get_option('website_name', get_bloginfo('name'));
	}
	?> <?php wp_title(); ?></title>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<link rel="icon" type="image/png" href="<?php
	if (function_exists('ot_get_option')) { echo ot_get_option('favicon_image', get_template_directory_uri() . '/img/logo-polda-mini.png');
	}
	?>">

	<!-- Google Analytics Code -->
	<?php
	if (function_exists('ot_get_option')) { echo ot_get_option('google_analytics_code');
	}
	?>

	<?php
	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
	?>
	</head>

	<body>

		<section id="top-nav">
			
			<div class="container-fluid search-form-wrapper" style="display:none;">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="row">
									<form class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">
										<input type="text" name="s" placeholder="Type here and hit enter.." />
										<input type="submit" value="Search" />
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<div class="row">
							<div class="pull-left">
								<a class="socmed-top-nav hvr-bounce-to-bottom" href="#"> <img src="<?php echo get_template_directory_uri(); ?>/img/fb-icon.png" alt="Facebook Icon" /> </a>
								<a class="socmed-top-nav hvr-bounce-to-bottom" href="#"> <img src="<?php echo get_template_directory_uri(); ?>/img/twitter-icon.png" alt="Twitter Icon" /> </a>
								<a class="socmed-top-nav hvr-bounce-to-bottom" href="#"> <img src="<?php echo get_template_directory_uri(); ?>/img/gplus-icon.png" alt="G-Plus Icon" /> </a>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<div class="row">
							<div class="pull-right">
								<a class="search-icon hvr-bounce-to-bottom" href="#"><i class="fa fa-search fa-lg" aria-hidden="true"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</section>

		<section id="header">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 logo">
						<div class="row">
							<a href="<?php echo esc_url(home_url()); ?>"> <img src="<?php echo get_template_directory_uri() . '/img/logo-polres-mini.png'; ?>" alt="<?php echo get_bloginfo('name'); ?>" width="" height="" /> </a>
						</div>
					</div>
					<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
						<div class="row">
							<h4 class="tribrata-title">Berita Terbaru <span>Tribrata News</span></h4>
							<div id="tribrata-news">
								<?php echo get_recent_news(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<nav class="navbar navbar-custom container">
			<div class="row">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="fa fa-lg fa-bars"></span>
					</button>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse row" id="navbar-collapse-1">
					<?php

					//* Primary navigation */
					wp_nav_menu(array('menu' => 'top_menu', 'depth' => 2, 'container' => false, 'menu_class' => 'nav navbar-nav',
					//Process nav menu using our custom nav walker
					'walker' => new wp_bootstrap_navwalker()));
					?>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container -->
		</nav>

