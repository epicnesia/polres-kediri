<?php get_header(); ?>

<section id="headlines">
	<div class="container">

		<?php echo nivo_slider(); ?>
		
	</div>
</section>

<section id="content">
	<div class="container">
		<div class="row content-wrapper">

			<?php get_sidebar(); ?>

			<div class="<?php echo is_active_sidebar('sidebar-1') == true ? 'col-lg-8 col-md-8 col-sm-6 col-xs-12' : 'col-lg-12 col-md-12 col-sm-12 col-xs-12'; ?> content">

				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"> <!-- Start Sub Content -->
					<h4 class="subcontent-title">Berita</h4>
					<div class="row post-article">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<?php

							$args = array('category_name' => 'berita'); //function_exists('ot_get_option') ? array('author' => ot_get_option('user_demo_1')) : array('author' => '1');
							$args['posts_per_page'] = '1';
							$args['nopagging'] = true;

							// Sub Section Query
							$sub_section = new WP_Query($args);

							// Berita Smada Loop
							while ($sub_section -> have_posts()) {
								$sub_section -> the_post();

								$post_id = $post -> ID;

								$the_url = wp_get_attachment_image_src(get_post_thumbnail_id($post -> ID), 'thumbnail');
								$img_src = isset($the_url[0]) ? $the_url[0] : get_template_directory_uri() . '/img/placeholder.jpg';
								echo '<a href="' . get_permalink() . '"><img src="' . $img_src . '" class="img-responsive content-image" alt="Featured Image of ' . get_the_title() . '" /></a>';
								echo '<a href="' . get_permalink() . '"><h4 class="post-title">' . get_the_title() . '</h4></a>';
								echo '<div class="post-data">
												By <a href="' . get_author_posts_url(get_the_author_meta('ID')) . '">' . get_the_author_meta('display_name') . '</a> /
												<i class="post-time">' . human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago</i>
											  </div>';
							}

							wp_reset_postdata();
							?>
						</div>
					</div>

					<?php

					// Related Post
					$args = array('category_name' => 'berita'); // function_exists('ot_get_option') ? array('author' => ot_get_option('user_demo_1')) : array('author' => '1');
					$args['posts_per_page'] = '3';
					$args['nopagging'] = true;
					$args['post__not_in'] = array($post_id);

					$sub_section = new WP_Query($args);
					
					?>

					<div class="row post-related-article">
						<ul class="list-unstyled col-lg-12 col-md-12 col-sm-12 col-xs-12">
							
							<?php
							// Related Post
							while ($sub_section -> have_posts()) {
								$sub_section -> the_post();

								echo '
							<li>
								<a href="' . get_permalink() . '" class="post-title">' . get_the_title() . '</a> / 
								<i class="post-time">'.human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago</i>
							</li>';
							}

							wp_reset_postdata();
							?>
						</ul>
					</div>
					
					<div class="row">
						<div class="more-link col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<?php
							    // Get the ID of a given category
							    $category_id = get_cat_ID( 'Berita' );
							
							    // Get the URL of this category
							    $category_link = get_category_link( $category_id );
							?>
							
							<!-- Print a link to this category -->
							<a href="<?php echo esc_url( $category_link ); ?>">View More</a>
							<?php // echo '<a href="' . get_author_posts_url(function_exists('ot_get_option') ? ot_get_option('user_demo_1') : '1'). '">View More</a>'; ?>
						</div>
					</div>

				</div> <!-- End Sub Content -->
				
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"> <!-- Start Sub Content -->
					<h4 class="subcontent-title">Info <span>Publik</span></h4>
					<div class="row post-article">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<?php

							$args = array('category_name' => 'info-publik'); // function_exists('ot_get_option') ? array('author' => ot_get_option('user_demo_2')) : array('author' => '1');
							$args['posts_per_page'] = '1';
							$args['nopagging'] = true;

							// Sub Section Query
							$sub_section = new WP_Query($args);

							// Berita Smada Loop
							while ($sub_section -> have_posts()) {
								$sub_section -> the_post();

								$post_id = $post -> ID;

								$the_url = wp_get_attachment_image_src(get_post_thumbnail_id($post -> ID), 'thumbnail');
								$img_src = isset($the_url[0]) ? $the_url[0] : get_template_directory_uri() . '/img/placeholder.jpg';
								echo '<a href="' . get_permalink() . '"><img src="' . $img_src . '" class="img-responsive content-image" alt="Featured Image of ' . get_the_title() . '" /></a>';
								echo '<a href="' . get_permalink() . '"><h4 class="post-title">' . get_the_title() . '</h4></a>';
								echo '<div class="post-data">
												By <a href="' . get_author_posts_url(get_the_author_meta('ID')) . '">' . get_the_author_meta('display_name') . '</a> /
												<i class="post-time">'.human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago</i>
											  </div>';
							}

							wp_reset_postdata();
							?>
						</div>
					</div>

					<?php

					// Related Post
					$args = array('category_name' => 'info-publik'); // function_exists('ot_get_option') ? array('author' => ot_get_option('user_demo_2')) : array('author' => '1');
					$args['posts_per_page'] = '3';
					$args['nopagging'] = true;
					$args['post__not_in'] = array($post_id);

					$sub_section = new WP_Query($args);
					
					?>

					<div class="row post-related-article">
						<ul class="list-unstyled col-lg-12 col-md-12 col-sm-12 col-xs-12">
							
							<?php
							// Related Post of Berita Smada Loop
							while ($sub_section -> have_posts()) {
								$sub_section -> the_post();

								echo '
							<li>
								<a href="' . get_permalink() . '" class="post-title">' . get_the_title() . '</a> / 
								<i class="post-time">'.human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago</i>
							</li>';
							}

							wp_reset_postdata();
							?>
						</ul>
					</div>
					
					<div class="row">
						<div class="more-link col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<?php
							    // Get the ID of a given category
							    $category_id = get_cat_ID( 'Info Publik' );
							
							    // Get the URL of this category
							    $category_link = get_category_link( $category_id );
							?>
							
							<!-- Print a link to this category -->
							<a href="<?php echo esc_url( $category_link ); ?>">View More</a>
							<?php //echo '<a href="' . get_author_posts_url(function_exists('ot_get_option') ? ot_get_option('user_demo_2') : '1'). '">View More</a>'; ?>
						</div>
					</div>

				</div> <!-- End Sub Content -->
				
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"> <!-- Start Sub Content -->
					<h4 class="subcontent-title">Sosial</span></h4>
					<div class="row post-article">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<?php

							$args = array('category_name' => 'sosial'); // function_exists('ot_get_option') ? array('author' => ot_get_option('user_demo_3')) : array('author' => '1');
							$args['posts_per_page'] = '1';
							$args['nopagging'] = true;

							// Sub Section Query
							$sub_section = new WP_Query($args);

							// Berita Smada Loop
							while ($sub_section -> have_posts()) {
								$sub_section -> the_post();

								$post_id = $post -> ID;

								$the_url = wp_get_attachment_image_src(get_post_thumbnail_id($post -> ID), 'thumbnail');
								$img_src = isset($the_url[0]) ? $the_url[0] : get_template_directory_uri() . '/img/placeholder.jpg';
								echo '<a href="' . get_permalink() . '"><img src="' . $img_src . '" class="img-responsive content-image" alt="Featured Image of ' . get_the_title() . '" /></a>';
								echo '<a href="' . get_permalink() . '"><h4 class="post-title">' . get_the_title() . '</h4></a>';
								echo '<div class="post-data">
												By <a href="' . get_author_posts_url(get_the_author_meta('ID')) . '">' . get_the_author_meta('display_name') . '</a> /
												<i class="post-time">'.human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago</i>
											  </div>';
							}

							wp_reset_postdata();
							?>
						</div>
					</div>

					<?php

					// Related Post
					$args = array('category_name' => 'sosial'); // function_exists('ot_get_option') ? array('author' => ot_get_option('user_demo_3')) : array('author' => '1');
					$args['posts_per_page'] = '3';
					$args['nopagging'] = true;
					$args['post__not_in'] = array($post_id);

					$sub_section = new WP_Query($args);
					
					?>

					<div class="row post-related-article">
						<ul class="list-unstyled col-lg-12 col-md-12 col-sm-12 col-xs-12">
							
							<?php
							// Related Post
							while ($sub_section -> have_posts()) {
								$sub_section -> the_post();

								echo '
							<li>
								<a href="' . get_permalink() . '" class="post-title">' . get_the_title() . '</a> / 
								<i class="post-time">'.human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago</i>
							</li>';
							}

							wp_reset_postdata();
							?>
						</ul>
					</div>
					
					<div class="row">
						<div class="more-link col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<?php
							    // Get the ID of a given category
							    $category_id = get_cat_ID( 'Sosial' );
							
							    // Get the URL of this category
							    $category_link = get_category_link( $category_id );
							?>
							
							<!-- Print a link to this category -->
							<a href="<?php echo esc_url( $category_link ); ?>">View More</a>
							<?php // echo '<a href="' . get_author_posts_url(function_exists('ot_get_option') ? ot_get_option('user_demo_3') : '1'). '">View More</a>'; ?>
						</div>
					</div>

				</div> <!-- End Sub Content -->

			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>