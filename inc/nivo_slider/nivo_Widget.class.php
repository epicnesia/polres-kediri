<?php

class nivo_Widget extends WP_Widget {
 
    public function __construct() {
        parent::__construct('nivo_Widget', 'Nivo Slideshow', array('description' => __('A Nivo Slideshow Widget', 'smadapare')));
    }
	
	public function form($instance) {
	    if (isset($instance['title'])) {
	        $title = $instance['title'];
	    }
	    else {
	        $title = __('Headlines Slideshow', 'smadapare');
	    }
	    ?>
	        <p>
	            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
	            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
	        </p>
	    <?php
	}
	
	public function update($new_instance, $old_instance) {
	    $instance = array();
	    $instance['title'] = strip_tags($new_instance['title']);
	 
	    return $instance;
	}
	
	public function widget($args, $instance) {
	    extract($args);
	    // the title
	    $title = apply_filters('widget_title', $instance['title']);
	    echo $before_widget;
	    if (!empty($title))
	        echo $before_title . $title . $after_title;
	    echo nivo_slider('nivo_widget');
	    echo $after_widget;
	}
	
}

// Nivo Slider Widget
add_action('widgets_init', 'nivo_widgets_init');
function nivo_widgets_init() {
	register_widget('nivo_Widget');
}

?>