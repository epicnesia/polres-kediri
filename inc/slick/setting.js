$(document).ready(function() {
	$('#tribrata-news').slick({
		autoplay : true,
		slidesToShow : 3,
		slidesToScroll : 1,
		responsive : [{
			breakpoint : 1200,
			settings : {
				slidesToShow : 2,
				slidesToScroll : 1,
			}
		}, {
			breakpoint : 992,
			settings : {
				slidesToShow : 2,
				slidesToScroll : 1
			}
		}, {
			breakpoint : 767,
			settings : {
				slidesToShow : 2,
				slidesToScroll : 1
			}
		}, {
			breakpoint : 480,
			settings : {
				slidesToShow : 1,
				slidesToScroll : 1
			}
		}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
		]
	});
});
