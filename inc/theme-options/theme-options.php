<?php
/**
 * Initialize the custom Theme Options.
 */
add_action( 'init', 'custom_theme_options' );

/**
 * Build the custom settings & update OptionTree.
 *
 * @return    void
 * @since     2.0
 */
function custom_theme_options() {

  /* OptionTree is not loaded yet, or this is not an admin request */
  if ( ! function_exists( 'ot_settings_id' ) || ! is_admin() )
    return false;

  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( ot_settings_id(), array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
  $custom_settings = array( 
    'contextual_help' => array( 
      'content'       => array( 
        array(
          'id'        => 'general_help',
          'title'     => __( 'General', 'polreskediri' ),
          'content'   => '<p>' . __( 'Theme option of Polres Kediri Official template!', 'polreskediri' ) . '</p>'
        )
      ),
    ),
    'sections'        => array( 
      array(
        'id'          => 'general_section',
        'title'       => __( 'General', 'polreskediri' )
      ),
      array(
        'id'          => 'users_section',
        'title'       => __( 'Users', 'polreskediri' )
      ),
      array(
        'id'          => 'categories_section',
        'title'       => __( 'Categories', 'polreskediri' )
      ),
      array(
        'id'          => 'contact_section',
        'title'       => __( 'Contact', 'polreskediri' )
      ),
      array(
        'id'          => 'social_media_section',
        'title'       => __( 'Social Media', 'polreskediri' )
      )
    ),
    'settings'        => array( 
      array(
        'id'          => 'logo_image',
        'label'       => __( 'Logo Image', 'polreskediri' ),
        'desc'        => sprintf( __( 'The Official website Logo' ) ),
        'std'         => get_template_directory_uri() .'/img/logo-polres-mini.png',
        'type'        => 'upload',
        'section'     => 'general_section',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'favicon_image',
        'label'       => __( 'Favicon Image', 'polreskediri' ),
        'desc'        => sprintf( __( 'The official website Favicon' ) ),
        'std'         => get_template_directory_uri() .'/img/logo-polda-mini.png',
        'type'        => 'upload',
        'section'     => 'general_section',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'website_name',
        'label'       => __( 'Website Name', 'polreskediri' ),
        'desc'        => sprintf( __( 'The website name' ) ),
        'std'         => 'Polres Kediri',
        'type'        => 'text',
        'section'     => 'general_section',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'google_analytics_code',
        'label'       => __( 'Google Analytics Code', 'polreskediri' ),
        'desc'        => sprintf( __( 'Google Analytics Code' ) ),
        'std'         => '',
        'type'        => 'textarea_simple',
        'section'     => 'general_section',
        'rows'        => '10',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'user_demo_1',
        'label'       => __( 'Demo 1', 'polreskediri' ),
        'desc'        => sprintf( __( 'This is the user to show posts in "Demo"' ) ),
        'std'         => '',
        'type'        => 'select',
        'section'     => 'users_section',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => my_users_list()
      ),
      array(
        'id'          => 'user_demo_2',
        'label'       => __( 'Demo 2', 'polreskediri' ),
        'desc'        => sprintf( __( 'This is the user to show posts in "Demo"' ) ),
        'std'         => '',
        'type'        => 'select',
        'section'     => 'users_section',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => my_users_list()
      ),
      array(
        'id'          => 'user_demo_3',
        'label'       => __( 'Demo 3', 'polreskediri' ),
        'desc'        => sprintf( __( 'This is the user to show posts in "Demo"' ) ),
        'std'         => '',
        'type'        => 'select',
        'section'     => 'users_section',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => my_users_list()
      ),
      array(
        'id'          => 'cat_headlines',
        'label'       => __( 'Headlines', 'polreskediri' ),
        'desc'        => sprintf( __( 'This is the category to show posts in "Headlines" or "Slider" section' ) ),
        'std'         => '',
        'type'        => 'category_select',
        'section'     => 'categories_section',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'cat_headlines_widget',
        'label'       => __( 'Headlines Widget', 'polreskediri' ),
        'desc'        => sprintf( __( 'This is the category to show posts in "Headlines Widget" or "Slider on sidebar" section' ) ),
        'std'         => '',
        'type'        => 'category_select',
        'section'     => 'categories_section',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'address',
        'label'       => __( 'Address', 'polreskediri' ),
        'desc'        => sprintf( __( 'The Address' ) ),
        'std'         => 'Jalan Jendral Sudirman, Pare, Kediri',
        'type'        => 'textarea_simple',
        'section'     => 'contact_section',
        'rows'        => '10',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'website',
        'label'       => __( 'Website', 'polreskediri' ),
        'desc'        => sprintf( __( 'The Website' ) ),
        'std'         => 'http://polreskediri.id',
        'type'        => 'text',
        'section'     => 'contact_section',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'email',
        'label'       => __( 'Email', 'polreskediri' ),
        'desc'        => sprintf( __( 'The Email' ) ),
        'std'         => 'info@polreskediri.id',
        'type'        => 'text',
        'section'     => 'contact_section',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'phone',
        'label'       => __( 'Phone', 'polreskediri' ),
        'desc'        => sprintf( __( 'The phone' ) ),
        'std'         => '(0354) 391001',
        'type'        => 'text',
        'section'     => 'contact_section',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'facebook_link',
        'label'       => __( 'Facebook Link', 'polreskediri' ),
        'desc'        => sprintf( __( 'The facebook link' ) ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social_media_section',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'twitter_link',
        'label'       => __( 'Twitter Link', 'polreskediri' ),
        'desc'        => sprintf( __( 'The twitter link' ) ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social_media_section',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'google_plus_link',
        'label'       => __( 'Google+ Link', 'polreskediri' ),
        'desc'        => sprintf( __( 'The google+ link' ) ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social_media_section',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
    )
  );
  
  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( ot_settings_id() . '_args', $custom_settings );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( ot_settings_id(), $custom_settings ); 
  }
  
  /* Lets OptionTree know the UI Builder is being overridden */
  global $ot_has_custom_theme_options;
  $ot_has_custom_theme_options = true;
  
}

function my_users_list(){
		
	$return = array(
		array(
			'value'       => '',
        	'label'       => __( '-- Choose One --', 'polreskediri' )
		)
	);
	
	global $wpdb;
	
	$wp_user_search = $wpdb->get_results("SELECT ID, display_name FROM $wpdb->users ORDER BY ID");
	foreach ( $wp_user_search as $userid ) {
		$result = array(
			'value'       => (int) $userid->ID,
	        'label'       => __( stripslashes($userid->display_name), 'polreskediri' )
		);
		array_push($return, $result);
	}
	
	return $return;
}
