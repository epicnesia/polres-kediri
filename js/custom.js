$(document).ready(function() {

	//============================= Bootstrap navbar active dropdown on hover ==================================
	if ($(window).width() > 767) {
		$('.navbar .dropdown').hover(function() {
			$(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

		}, function() {
			$(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();

		});

		$('.navbar .dropdown > a').click(function() {
			location.href = this.href;
		});

	}
	
	//============================= Prevent title show up on hover ==================================
	var links = document.getElementsByTagName("a");
    DisableToolTip(links);
    
	function DisableToolTip(elements) {
	    for (var i = 0; i < elements.length; i++) {
	        var element = elements[i];
	        element.onmouseover = function() {
	            this.setAttribute("org_title", this.title);
	            this.title = "";
	        };
	        element.onmouseout = function() {
	            this.title = this.getAttribute("org_title");
	        };
	    }
	}
    
    
    //============================= Sidebar height matcher ==================================
    if($(window).width() > 767){
	    var conHeight = $('.content').height();
	    var sideHeight = $('#sidebar').height();
	    if(conHeight > sideHeight){
	    	$('#sidebar').height(conHeight);
	    }
	}
	
	//============================= Slide Toggle Search Form ==================================
	$( ".search-icon" ).click(function() {
	  $( ".search-form-wrapper" ).slideToggle( "slow" );
	})

});
