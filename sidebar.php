<?php
/**
 * The sidebar containing the primary widget area
 *
 * Displays on posts and pages.
 *
 * If no active widgets are in this sidebar, hide it completely.
 *
 * @package WordPress
 * @subpackage PolresKediri
 * @since PolresKediri 1.0
 */

if ( is_active_sidebar( 'sidebar-1' ) ) :
?>
<div id="sidebar" class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
	<?php dynamic_sidebar('sidebar-1'); ?>
</div>
<?php endif; ?>