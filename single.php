<?php get_header(); ?>

<div class="container">
	<div class="row content-wrapper">

		<?php 
		
		if (have_posts()) : while (have_posts()) : the_post(); 
				
			$the_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'small');
		
			if(isset($the_url[0])) :
				
		?>
		
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="row">
				<img src="<?php echo $the_url[0]; ?>" class="featured-image" width="" height="" alt="Featured image of <?php the_title(); ?>" />
			</div>
		</div>
		
		<?php else: ?>
			
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="row">
				<img src="<?php echo get_template_directory_uri() . '/img/placeholder.jpg'; ?>" class="featured-image" width="" height="" alt="Featured image of <?php the_title(); ?>" />
			</div>
		</div>
		
		<?php endif; ?>
		
		<div class="<?php echo is_active_sidebar('sidebar-1') == true ? 'col-lg-8 col-md-8 col-sm-6 col-xs-12' : 'col-lg-12 col-md-12 col-sm-12 col-xs-12'; ?> content">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 post-content-wrapper">
				<h2 class="post-title"><?php the_title(); ?></h2>
				<div class="post-data">
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><i class="fa fa-user"></i> <?php echo get_the_author_meta('display_name'); ?></a>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<a href="<?php echo get_day_link(my_date(get_the_date('d-m-Y'), 'year'), my_date(get_the_date('d-m-Y'), 'month'), my_date(get_the_date('d-m-Y'), 'day')); ?>"><i class="fa fa-calendar"></i> <?php echo my_date(get_the_date()); ?></a>
						</div>
					</div>
				</div>
				<div class="full-divider"></div>
				<?php the_content(); ?>
				<div class="full-divider"></div>
				<div class="row">
				<?php 
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif; 
				?>
				</div>
			</div>
		</div>
		
		<?php get_sidebar(); ?>
		
		<?php endwhile; ?>
		<?php endif; ?>
	</div>
</div>

<?php get_footer(); ?>